/** @file: tokenizer.cpp
	@author Garth Santor/Trinh Han
	@author garth.santor@sympatico.ca
	@author http://www.gats.ca/
	@version 0.2.0
	@date 2012-11-16
	@note Compiles under Visaul C++ v110

	@brief Tokenizer class implementation.
	*/

#include "tokenizer.hpp"
#include "boolean.hpp"
#include "function.hpp"
#include "integer.hpp"
#include "operator.hpp"
#include "pseudo_operation.hpp"
#include "real.hpp"
#include "variable.hpp"

#include <exception>
#include <limits>
#include <sstream>
#include <stack>
#include <string>
using namespace std;

/** Default constructor loads the keyword dictionary. */
Tokenizer::Tokenizer() {
	keywords_["abs"]     = keywords_["Abs"]		= keywords_["ABS"]		= make<Abs>();
	keywords_["mod"]	 = keywords_["Mod"]		= keywords_["MOD"]		= make<Modulus>();
	keywords_["pi"]		 = keywords_["Pi"]		= keywords_["PI"]		= make<Pi>();
	keywords_["e"]		 = keywords_["E"]								= make<E>();
	keywords_["arccos"]	 = keywords_["Arccos"]	= keywords_["ARCCOS"]	= make<Arccos>();
	keywords_["arcsin"]  = keywords_["Arcsin"]  = keywords_["ARCSIN"]   = make<Arcsin>();
	keywords_["arctan"]  = keywords_["Arctan"]  = keywords_["ARCTAN"]	= make<Arctan>();
	keywords_["ceil"]	 = keywords_["Ceil"]	= keywords_["CEIL"]		= make<Ceil>();
	keywords_["cos"]	 = keywords_["Cos"]		= keywords_["COS"]		= make<Cos>();
	keywords_["exp"]	 = keywords_["Exp"]		= keywords_["EXP"]		= make<Exp>();
	keywords_["floor"]   = keywords_["Floor"]   = keywords_["FLOOR"]	= make<Floor>();
	keywords_["lb"]		 = keywords_["LB"]								= make<Lb>();
	keywords_["ln"]		 = keywords_["LN"]								= make<Ln>();
	keywords_["log"]     = keywords_["Log"]		= keywords_["LOG"]		= make<Log>();
	keywords_["sin"]	 = keywords_["Sin"]		= keywords_["SIN"]		= make<Sin>();
	keywords_["sqrt"]    = keywords_["Sqrt"]    = keywords_["SQRT"]		= make<Sqrt>();
	keywords_["tan"]	 = keywords_["Tan"]		= keywords_["TAN"]		= make<Tan>();
	keywords_["arctan2"] = keywords_["Arctan2"] = keywords_["ARCTAN2"]  = make<Arctan2>();
	keywords_["max"]	 = keywords_["Max"]		= keywords_["MAX"]      = make<Max>();
	keywords_["min"]	 = keywords_["Min"]		= keywords_["MIN"]		= make<Min>();
	keywords_["pow"]	 = keywords_["Pow"]		= keywords_["POW"]		= make<Pow>();
	keywords_["not"]	 = keywords_["Not"]		= keywords_["NOT"]		= make<Not>();
	keywords_["and"]	 = keywords_["And"]		= keywords_["AND"]		= make<And>();
	keywords_["or"]		 = keywords_["Or"]		= keywords_["OR"]		= make<Or>();
	keywords_["xor"]	 = keywords_["Xor"]		= keywords_["XOR"]		= make<Xor>();
	keywords_["nand"]    = keywords_["Nand"]	= keywords_["NAND"]		= make<Nand>();
	keywords_["nor"]     = keywords_["Nor"]		= keywords_["NOR"]		= make<Nor>();
	keywords_["xnor"]	 = keywords_["Xnor"]	= keywords_["XNOR"]		= make<Xnor>();
	keywords_["false"]   = keywords_["False"]	= keywords_["FALSE"]	= make<False>();
	keywords_["true"]    = keywords_["True"]    = keywords_["TRUE"]		= make<True>();
	keywords_["result"] = keywords_["Result"] = keywords_["RESULT"] = make<Result>();
}




/** Get an identifier from the expression.
	Assumes that the currentChar is pointing to a alphabetic.
	*/
Token::pointer_type Tokenizer::_get_identifier( Tokenizer::string_type::const_iterator& currentChar, 
												Tokenizer::string_type const& expression,
												TokenList tl) 
{
	// accumulate identifier
	string_type ident;
	do
		ident += *currentChar++;
	while( currentChar != end(expression) && isalnum( *currentChar ) );

	// check for predefined identifier
	dictionary_type::iterator iter = keywords_.find(ident);
	if( iter != end(keywords_) )
		return iter->second;


	if (tl.size() == 0) {
		if (variables_.find(ident) == variables_.cend())
			variables_.insert(make_pair(ident, make<Variable>()));

		return variables_[ident];
	}

	throw XBadCharacter(expression, currentChar - begin(expression));
}




/** Get a number token from the expression.
	@return One of BinaryInteger, Integer, or Real.
	@param currentChar [in,out] an iterator to the current character.  Assumes that the currentChar is pointing to a digit.
	@param expression [in] the expression being scanned.
*/
Token::pointer_type Tokenizer::_get_number( Tokenizer::string_type::const_iterator& currentChar, Tokenizer::string_type const& expression ) {
	assert( isdigit( *currentChar ) && "currentChar must pointer to a digit" );
	string_type ident;
	bool isReal = false;
	while (currentChar != expression.cend()  && (isdigit(*currentChar) || *currentChar == '.')) 
	{ 
		if (*currentChar == '.') { isReal = true; }
		else if (isdigit(*currentChar)){ }
		else
			break;

		ident += *currentChar++;
	}

	if(isReal)
		return make<Real>(ident);

	return make<Integer>(ident);
}



/** Tokenize the expression.
	@return a TokenList containing the tokens from 'expression'. 
	@param expression [in] The expression to tokenize.
	@note Tokenizer dictionary may be updated if expression contains variables.
	@note Will throws 'BadCharacter' if the expression contains an un-tokenizable character.
	*/
TokenList Tokenizer::tokenize(string_type const& expression) {
	TokenList tokenizedExpression;
	auto currentChar = expression.cbegin();

	for(;;) 
	{
		// strip whitespace
		while( currentChar != end(expression) && isspace(*currentChar) )
			++currentChar;

		// check of end of expression
		if( currentChar == end(expression) ) break;

		// check for a number
		if( isdigit( *currentChar ) ) {
			tokenizedExpression.push_back( _get_number( currentChar, expression) );
			continue;
		}

		if (isalnum(*currentChar)) {
			tokenizedExpression.push_back(_get_identifier(currentChar, expression,tokenizedExpression));
			continue;
		}

		if (*currentChar == '+') {
			
			if (tokenizedExpression.size() != 0 &&  
				(is<RightParenthesis>(*(tokenizedExpression.cend() - 1)) ||
				is<Operand>(*(tokenizedExpression.cend() - 1))))
					tokenizedExpression.push_back(make<Addition>());
			else		
				tokenizedExpression.push_back(make<Identity>());

			++currentChar;
			continue;
		}

		if (*currentChar == '-') {

			if (tokenizedExpression.size() != 0 &&
				(is<RightParenthesis>(*(tokenizedExpression.cend() - 1)) ||
					is<Operand>(*(tokenizedExpression.cend() - 1))))
				tokenizedExpression.push_back(make<Subtraction>());
			else
				tokenizedExpression.push_back(make<Negation>());

			++currentChar;
			continue;
		}

		if (*currentChar == '!') {		
			if (currentChar != expression.cend() - 1 && *(next(currentChar, 1)) == '=') {
				tokenizedExpression.push_back(make<Inequality>());
				currentChar += 2;	
			}
			else {
				tokenizedExpression.push_back(make<Factorial>());
				++currentChar;
			}
			continue;
		}

		if (*currentChar == '*') {
			tokenizedExpression.push_back(make<Multiplication>());
			++currentChar;
			continue;
		}

		if (*currentChar == '/') {
			tokenizedExpression.push_back(make<Division>());
			++currentChar;
			continue;
		}

		if (*currentChar == '%') {
			tokenizedExpression.push_back(make<Modulus>());
			++currentChar;
			continue;
		}

		if (*currentChar == '#') {
			tokenizedExpression.push_back(make<Power>());
			++currentChar;
			continue;
		}

		if (*currentChar == '(') {
			tokenizedExpression.push_back(make<LeftParenthesis>());
			++currentChar;
			continue;
		}

		if (*currentChar == ')') {
			tokenizedExpression.push_back(make<RightParenthesis>());
			++currentChar;
			continue;
		}

		if (*currentChar == ',') {
			tokenizedExpression.push_back(make<ArgumentSeparator>());
			++currentChar;
			continue;
		}

		if (*currentChar == '=') {
			if (currentChar != expression.cend() && *(next(currentChar, 1)) == '=') {
				tokenizedExpression.push_back(make<Equality>());
				currentChar += 2;
			}
			else {
				tokenizedExpression.push_back(make<Assignment>());
				++currentChar;
			}
			continue;
		}

		if (*currentChar == '>') {
			if (currentChar != expression.cend() && *(next(currentChar, 1)) == '=') {
				tokenizedExpression.push_back(make<GreaterEqual>());
				currentChar += 2;
			}
			else {
				tokenizedExpression.push_back(make<Greater>());
				++currentChar;
			}
			continue;
		}

		if (*currentChar == '<') {
			if (currentChar != expression.cend() && *(next(currentChar, 1)) == '=') {
				tokenizedExpression.push_back(make<LessEqual>());
				currentChar += 2;
			}
			else {
				tokenizedExpression.push_back(make<Less>());
				++currentChar;
			}
			continue;
		}

		// not a recognized token
		throw XBadCharacter( expression, currentChar - begin(expression) );
	}

	return tokenizedExpression;
}

/*=============================================================

Revision History

Version 0.2.0: 2012-11-16
Added BitAnd, BitNot, BitOr, BitXOr, BitShiftLeft, BitShiftRight
Simplified CHECK_OP macros

Version 0.1.0: 2012-11-15
Replaced BadCharacter with XTokenizer, XBadCharacter, and XNumericOverflow
Added BinaryInteger, Binary <function>

Version 0.0.1: 2012-11-13
C++ 11 cleanup

Version 0.0.0: 2009-11-25
Alpha release.

=============================================================

Copyright Garth Santor/Trinh Han

The copyright to the computer program(s) herein
is the property of Garth Santor/Trinh Han of Canada.
The program(s) may be used and/or copied only with
the written permission of Garth Santor/Trinh Han
or in accordance with the terms and conditions
stipulated in the agreement/contract under which
the program(s) have been supplied.
=============================================================*/
