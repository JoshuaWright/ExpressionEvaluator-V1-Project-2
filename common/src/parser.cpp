/** @file: parser.cpp
	@author Garth Santor/Trinh Han
	@author garth.santor@sympatico.ca
	@author http://www.gats.ca/
	@version 1.0.0
	@date 2012-11-13
	@note Compiles under Visaul C++ v120

	@brief Parser class implementation.
	*/

#include "parser.hpp"
#include "function.hpp"
#include "operand.hpp"
#include "operator.hpp"
#include "pseudo_operation.hpp"
#include <stack>
#include <queue>
using namespace std;


/* 
creates a TokenList in post-fix-notation
@return a TokenList in post-fix-notation
@param expression[in] TokenList
*/
TokenList Parser::parse(TokenList const& infixTokens) {

	stack<Token::pointer_type> operationStack;
	TokenList posfix;
	int index = -1;

	for each (Token::pointer_type tp in infixTokens)
	{
		index++;
		if ( is<Operand>(tp) )
			posfix.push_back(tp);
		else if ( is<Function>(tp) )
			operationStack.push(tp);
		else if (is<ArgumentSeparator>(tp))
		{
			while (!is<LeftParenthesis>(operationStack.top()) )
			{
				posfix.push_back(operationStack.top());
				operationStack.pop();
			}
		}
		else if (is<LeftParenthesis>(tp))
			operationStack.push(tp);
		else if (is<RightParenthesis>(tp))
		{
			while (!is<LeftParenthesis>(operationStack.top()))
			{
				posfix.push_back(operationStack.top());
				operationStack.pop();
			}

			if (operationStack.empty()) {
				throw XLeftParenthesis(index);
			}

			operationStack.pop();

			if (!operationStack.empty() && is<Function>(operationStack.top()))
			{
				posfix.push_back(operationStack.top());
				operationStack.pop();
			}
		}
		else if (is<Operator>(tp))
		{
			while (!operationStack.empty())
			{
				if (!is<Operator>(tp) || is<UnaryOperator>(tp))
					break;
				if (is<BinaryOperator>(operationStack.top())) {
					if (is<LAssocOperator>(tp) &&
						static_pointer_cast<LAssocOperator>(tp)->get_precedence() >
						static_pointer_cast<LAssocOperator>(operationStack.top())->get_precedence())
						break;
					else if (is<RAssocOperator>(tp) &&
						static_pointer_cast<RAssocOperator>(tp)->get_precedence() >=
						static_pointer_cast<RAssocOperator>(operationStack.top())->get_precedence())
						break;
					posfix.push_back(operationStack.top());
					operationStack.pop();
				}
				else break;
			}
			operationStack.push(tp);
		}
		else { throw XUnknowntoken(index); }
	}

	while (!operationStack.empty())
	{
		if( is<LeftParenthesis>(operationStack.top()) ) {
			throw XRightParenthesis(index);
		}
		posfix.push_back(operationStack.top());
		operationStack.pop();
	}

	return posfix;
}


/*=============================================================

Revision History
Version 1.0.0: 2014-10-31
Visual C++ 2013

Version 0.0.1: 2012-11-13
C++ 11 cleanup

Version 0.0.0: 2009-12-02
Alpha release.

=============================================================

Copyright Garth Santor/Trinh Han

The copyright to the computer program(s) herein
is the property of Garth Santor/Trinh Han of Canada.
The program(s) may be used and/or copied only with
the written permission of Garth Santor/Trinh Han
or in accordance with the terms and conditions
stipulated in the agreement/contract under which
the program(s) have been supplied.
=============================================================*/
