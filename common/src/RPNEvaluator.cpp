/** @file: RPNEvaluator.cpp
	@author Garth Santor/Trinh Han
	@author garth.santor@sympatico.ca
	@author http://www.gats.ca/
	@version 0.0.1
	@date 2012-11-13
	@note Compiles under Visaul C++ v110

	@brief RPN Evaluator class implementation.
	*/

#include "RPNEvaluator.hpp"
#include "integer.hpp"
#include "operation.hpp"
#include <cassert>
#include <stack>
#include <queue>


Operand::pointer_type RPNEvaluator::evaluate(TokenList const& rpnExpression) {
	std::stack<Operand::pointer_type> operandStack;
	std::stack<Operation::pointer_type> operationQ;

	return operandStack.top();
}

/*=============================================================

Revision History

Version 0.0.1: 2012-11-13
C++ 11 cleanup

Version 0.0.0: 2009-12-10
Alpha release.

=============================================================

Copyright Garth Santor/Trinh Han

The copyright to the computer program(s) herein
is the property of Garth Santor/Trinh Han of Canada.
The program(s) may be used and/or copied only with
the written permission of Garth Santor/Trinh Han
or in accordance with the terms and conditions
stipulated in the agreement/contract under which
the program(s) have been supplied.
=============================================================*/
