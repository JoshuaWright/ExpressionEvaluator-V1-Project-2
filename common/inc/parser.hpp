#ifndef GUARD_parser_hpp20091201_
#define GUARD_parser_hpp20091201_

/** @file: parser.hpp
	@author Garth Santor/Trinh Han
	@author garth.santor@sympatico.ca
	@author http://www.gats.ca/
	@version 1.0.0
	@date 2012-11-13
	@note Compiles under Visaul C++ v120

	@brief Parser class declaration.
	*/

#include <boost/noncopyable.hpp>
#include "token.hpp"
#include "tokenizer.hpp"

class Parser : boost::noncopyable {
public:

	/* creates a TokenList in post-fix-notation */
	TokenList parse( TokenList const& infixTokens );
 
	/* Base exception class for parser */
	class XParser : public std::exception {
		size_t		location_;
	public:
		XParser(size_t location, char const* msg)
			: std::exception(msg)
			, location_(location)
		{ }
		/** Gets the location of the bad character in the expression string. */
		size_t		get_location() const { return location_; }
	};

	/** missing Left Parenthesis exception class. */
	class XLeftParenthesis : public XParser {
	public:
		XLeftParenthesis(size_t location)
			: XParser(location, "Parser::Right parenthesis, has no matching left parenthesis.") { }
	};

	/** missing right Parenthesis exception class. */
	class XRightParenthesis : public XParser {
	public:
		XRightParenthesis(size_t location)
			: XParser(location, "Parser::Missing right-parenthesis.") { }
	};

	/** Unknown token�. exception class. */
	class XUnknowntoken : public XParser {
	public:
		XUnknowntoken(size_t location)
			: XParser(location, "Parser::Unknown token.") { }
	};
};

/*=============================================================

Revision History
Version 1.0.0: 2014-10-31
Visual C++ 2013

Version 0.0.1: 2012-11-13
C++ 11 cleanup

Version 0.0.0: 2009-12-01
Alpha release.

=============================================================

Copyright Garth Santor/Trinh Han

The copyright to the computer program(s) herein
is the property of Garth Santor/Trinh Han of Canada.
The program(s) may be used and/or copied only with
the written permission of Garth Santor/Trinh Han
or in accordance with the terms and conditions
stipulated in the agreement/contract under which
the program(s) have been supplied.
=============================================================*/
#endif // GUARD_parser_hpp20091201_
