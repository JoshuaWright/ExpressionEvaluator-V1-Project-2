#if !defined(GUARD_function_hpp20091126_)
#define GUARD_function_hpp20091126_

/** @file: function.hpp
	@author Garth Santor/Trinh Han
	@author garth.santor@sympatico.ca
	@author http://www.gats.ca/
	@version 1.0.0
	@date 2012-11-13
	@note Compiles under Visaul C++ v120

	@brief function token declarations.
	*/

#include "operation.hpp"
#include "integer.hpp"
#include <vector>

/** Function token base class. */
class Function : public Operation { };

		/** One argument function token base class. */
		class OneArgFunction : public Function {
		public:
			virtual unsigned number_of_args() const override { return 1; }
			//DECL_OPERATION_PERFORM();
		};

				/** Absolute value function token. */
				class Abs : public OneArgFunction {
				DEF_IS_CONVERTABLE_FROM(Abs)
				};

				/** Arccos value function token. */
				class Arccos : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Arccos)
				};

				/** Arcsin value function token. */
				class Arcsin : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Arcsin)
				};

				/** Arctan value function token. */
				class Arctan : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Arctan)
				};

				/** Ceil value function token. */
				class Ceil : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Ceil)
				};

				/** Cos value function token. */
				class Cos : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Cos)
				};

				/** Exp value function token. */
				class Exp : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Exp)
				};

				/** Floor value function token. */
				class Floor : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Floor)
				};

				/** Lb value function token. */
				class Lb : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Lb)
				};

				/** Ln value function token. */
				class Ln : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Ln)
				};

				/** Log value function token. */
				class Log : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Log)
				};

				/** Sin value function token. */
				class Sin : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Sin)
				};

				/** Sqrt value function token. */
				class Sqrt : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Sqrt)
				};

				/** Tan value function token. */
				class Tan : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Tan)
				};

				/** Result value function token. */
				class Result : public OneArgFunction {
					DEF_IS_CONVERTABLE_FROM(Result)
				};


		/** One argument function token base class. */
		class MultiArgFunction : public Function {
		public:
			virtual unsigned number_of_args() const override { return 2; }
			//DECL_OPERATION_PERFORM();
		};

				/** Max value function token. */
				class Max : public MultiArgFunction {
					DEF_IS_CONVERTABLE_FROM(Max)
				};

				/** Min value function token. */
				class Min : public MultiArgFunction {
					DEF_IS_CONVERTABLE_FROM(Min)
				};

				/** Pow value function token. */
				class Pow : public MultiArgFunction {
					DEF_IS_CONVERTABLE_FROM(Pow)
				};
				
				/** Arctan2 value function token. */
				class Arctan2 : public MultiArgFunction {
					DEF_IS_CONVERTABLE_FROM(Arctan2)
				};

/*=============================================================

Revision History

Version 1.0.0: 2016-11-02
Added 'override' keyword where appropriate.

Version 0.0.2: 2014-10-30
Removed binary function

Version 0.0.1: 2012-11-13
C++ 11 cleanup

Version 0.0.0: 2009-11-26
Alpha release.

=============================================================

Copyright Garth Santor/Trinh Han

The copyright to the computer program(s) herein
is the property of Garth Santor/Trinh Han of Canada.
The program(s) may be used and/or copied only with
the written permission of Garth Santor/Trinh Han
or in accordance with the terms and conditions
stipulated in the agreement/contract under which
the program(s) have been supplied.
=============================================================*/
#endif // GUARD_function_hpp20091126_
